package ooss;

import java.util.*;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private Set<Klass> klasses;

    public Teacher(int id, String name, int age) {
        super(id, name, age);
        this.klasses = new LinkedHashSet<>();
    }

    @Override
    public String introduce() {
        if (klasses.size() == 0) {
            return String.format("My name is %s. I am %d years old. I am a teacher.", name, age);
        } else {
            String klassList = this.klasses.stream()
                    .map(k -> "" + k.getNum())
                    .collect(Collectors.joining(", "));

            return String.format("My name is %s. I am %d years old. I am a teacher. I teach Class %s.", name, age, klassList);
        }

    }

    public void assignTo(Klass klass) {
        this.klasses.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return this.klasses.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return klasses.contains(student.getKlass());
    }

}
