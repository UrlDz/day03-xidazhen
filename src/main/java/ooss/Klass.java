package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private int num;
    private Student leader;
    private List<Student> students;

    public Klass() {
    }

    public Klass(int num) {
        this.num = num;
        this.students = new ArrayList<>();

    }

    public int getNum() {
        return num;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return num == klass.num;
    }

    @Override
    public int hashCode() {
        return Objects.hash(num);
    }

    public void assignLeader(Student leader) {

        if (this.students.contains(leader)) {
            this.leader = leader;


        } else {
            System.out.println("It is not one of us.");
        }

    }

    public void addStudent(Student student){
        if(students.contains(student)){
            return;
        }
        students.add(student);
    }

    public boolean isLeader(Student student) {
        return this.leader != null && this.leader.equals(student);
    }


    public void attach(Person person) {

    }
    
}
