package ooss;

public class Student extends Person {

    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
        klass = new Klass();
    }

    public Klass getKlass() {
        return klass;
    }

    @Override
    public String introduce() {
        if (this.klass.getNum() == 0) {
            return String.format("My name is %s. I am %d years old. I am a student.", name, age);
        }else {
            if (this.klass.isLeader(this)) {
                return String.format("My name is %s. I am %d years old. I am a student. I am the leader of class %d.", name, age, klass.getNum());
            }else {
                return String.format("My name is %s. I am %d years old. I am a student. I am in class %d.", name, age, klass.getNum());
            }
        }

    }

    public void join(Klass klass) {
        this.klass = klass;
        klass.addStudent(this);
    }

    public boolean isIn(Klass klass) {
        return klass.equals(this.klass);
    }

    public void notify(Klass klass, Student leader) {

    }
}
